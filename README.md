# vk

[![Build Status](https://travis-ci.org/toy80/vk.svg?branch=master)](https://travis-ci.org/toy80/vk)

Package vk is an experimental Vulkan binding for golang.

## Minimal example

Install the official vulkan runtime and run the toy80-example-vk package

## More examples

See the [github.com/toy80/toy](https://github.com/toy80/toy) repository, screenshot:

![Screenshot-0](https://github.com/toy80/toy/blob/master/doc/screenshot-0.png?raw=true)